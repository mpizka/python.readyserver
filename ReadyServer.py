# ReadyServer provides a simple interface to quickly set up a webserver
# mapping handler functions to paths

import fnmatch
import glob
import http.server
import json
import mimetypes
import os
import re
import ssl
import string
import sys
import threading
import time
import traceback
import urllib.parse as urlparse

from collections import namedtuple
from http import HTTPStatus
from socketserver import ThreadingMixIn


# constants
CTYPE_TEXT = "text/plain; charset=utf-8"
CTYPE_HTML = "text/html; charset=utf-8"
CTYPE_JSON = "application/json; charset=utf-8"
CTYPE_BYTES = "application/octet-stream"
CTYPE_NONE = ""


# threading support for http server class and handler definition
class ThreadingHTTP(ThreadingMixIn, http.server.HTTPServer):
    pass


class HTTPHandler(http.server.BaseHTTPRequestHandler):
    # Override socketserver.StreamRequestHandler timeout (defaults to None)
    # during .setup() this is set as the timeout of the connection attribute
    # which is the request (socket.accept() return) given by the server
    timeout = None
    
    # Maximum acceptable content length
    max_content_length = 2 ** 24 # 16MiB

    # override standard logger methods
    # you can link these up to self.server.log if you need them
    def log_request(self, *args):
        pass
    def log_error(self, *args):
        pass
    def log_message(self, *args):
        pass

    # send redirect to client
    def sendRedirect(self, location, code=308):
        self.send_response(code)
        self.send_header("Location", location)
        self.end_headers()
        self.server.log.debug(f"{self.server._name} redirect: {self.command} {self.path} -> {location}")

    # send error to client
    def sendErr(self, code, msg):
        # get phrase
        if code == 418:
            phrase = "I'm a teapot"
        else:
            phrase = HTTPStatus(code).phrase
        data, _ = MkJSON({"error": {"code": code, "phrase": phrase, "msg": msg}})
        self._send(data, code, CTYPE_JSON)

    # _send sends a response
    # par: data(bytes): data to send
    #      code(int): HTTP response code
    #      ctype(str) Content-Type
    def _send(self, data, code, ctype):
        # FIXME this could be better, check at doPOST, doGET instead?
        # FIXME autoconvert data(str) to data(bytes) using utf8 encoding
        if not (isinstance(data, (int, bytes)) and isinstance(code, int) and isinstance(ctype, str)):
            self.server.log.error(f"{self.server._name} bad handler return: data={data} code={code} ctype={ctype}")
            self.sendErr(500, "handler fail: bad handler data")
            return

        # set rcode + headers
        # (send_response and send_header write to internal buffer)
        self.send_response(code)   
        self.send_header('Content-Type', ctype)
        if isinstance(data, int):
            self.send_header('Content-Length', data)
        self.send_header('Content-Length', len(data))

        for k, v in self._addheaders.items():
            self.send_header(k, v)

        # write to socket
        self.end_headers()

        # honor HEAD verb even for dumb handler functions
        if self.command == "HEAD":
            self.server.log.debug(f"{self.server._name} send: {code} 0 {self.command} {self.path}")
            return

        # write to socket
        self.wfile.write(data)
        self.server.log.debug(f"{self.server._name} send: {code} {len(data)} {self.command} {self.path}")


    # AddHeader allows a handler-function to add custom headers to the response
    def AddHeader(self, key, val):
        if not(isinstance(key, str) and isinstance(val, str)):
            return
        self._addheaders[key] = val
    

    # GET, HEAD, POST, PUT, PATCH, DELETE
    def do_ALL(self):
        self._addheaders = {}
        # get urlpath & query
        parse = urlparse.urlparse(self.path)
        path = parse.path
        query = urlparse.parse_qs(parse.query)

        # get content length 
        if self.command in ["POST", "PUT", "PATCH", "DELETE"]:
            contentLength = int(self.headers.get('Content-Length', None))
            if (contentLength < 0 or contentLength > self.max_content_length or
               self.command != "DELETE" and contentLength == None):
                self.server.log.warn(f"{self.server._name} {self.command}: bad content length")
                self.sendErr(400, "bad Content-Length")
                return

            # try reading content, end on timeout
            try:
                rdata = self.rfile.read(contentLength)
            except Exception as err:
                self.server.log.error(f"{self.server._name} {self.command}: read error: {err}")
                return
        else:
            rdata = b""

        # resolve path to handler and handler-type
        handler, htype = self._resolve_path(path)
        if handler is None:
            return

        # file : handler is path relative to web-root
        # FIXME: current web-root is working directory
        if htype == "file":
            try:
                fdata, ctype = _get_file(os.path.join(os.getcwd(), handler))
                if ctype is None:
                    ctype = CTYPE_NONE
                self.AddHeader("Cache-Control", "private, max-age=7200")
                self._send(fdata, 200, ctype)
            except FileNotFoundError:
                self.server.log.error(f"{self.server._name} get file: '{path}' not found")
                self.sendErr(404, f"'{path}' not found")
            except Exception as err:
                self.server.log.error(f"{self.server._name} get file: error: {err}")
                self.sendErr(500, f"handler fail: {err}")
            finally:
                return

        # redirect: handler is redirect-path as string
        elif htype == "redirect":
            self.AddHeader("Location", handler)
            self.sendRedirect(handler)
            return

        # template: handler is (template_path, function)
        # temlate_path is relative to web-root
        # FIXME: current web-root is working directory
        elif htype == "template":
            try:
                template_path, func = handler
                fdata, ctype = _get_template(os.path.join(os.getcwd(), template_path), func)
                self._send(fdata, 200, ctype)
            except FileNotFoundError:
                self.server.log.error(f"{self.server._name} template: '{path}' not found")
                self.sendErr(404, f"'{path}' not found")
            except Exception as err:
                self.server.log.error(f"{self.server._name} template: error: {err}")
                self.sendErr(500, f"handler fail: {err}")
            finally:
                return

        # api: handler is a function
        elif htype == "api":
            try:
                fdata, code, ctype = handler(
                    server = self,
                    path = path,
                    headers = dict(self.headers.items()),
                    query = query,
                    data = rdata)
                self._send(fdata, code, ctype)
            except Exception as err:
                self.server.log.error(f"{self.server._name} {self.command}: handler exception\n{traceback.format_exc()}")
                self.sendErr(500, f"handler fail: {err}")
            finally:
                return

        # unknown handler type
        else:
            self.server.log.error(f"{self.server._name} bad handler type {htype} for path '{path}'")
            self.sendErr(500, f"bad handler type")
            return


    # resolve a path to a registered handler function and handler type
    def _resolve_path(self, path):
        # try static paths
        handler, htype = self.server._static_paths[self.command].get(path, (None, None))
        # try globpaths
        if handler is None:
            for gp, v in self.server._glob_paths[self.command].items():
                if fnmatch.fnmatch(path, gp):
                    handler, htype  = v
                    self.server.log.debug(f"_resolve_path: '{path}' is glob")
                    break
        # if path isn't registered, this returns None
        if handler is None:
            self.server.log.info(f"{self.server._name} {self.command}: unknown path: {path}")
            self.sendErr(404, "no such path")
        return handler, htype

    do_GET = do_ALL
    do_HEAD = do_ALL
    do_POST = do_ALL
    do_PUT = do_ALL
    do_PATCH = do_ALL
    do_DELETE = do_ALL


# QueryUnpack extracts a list of keys from a query as a named tuple
# returns an error if a key is missing
# par:  query: query as handed to handler functions
#       *keys: [str], list of keys in query. keys must be valid python identifiers
#       first_only: bool, optional, default True: return only first value
#                   of each key
# ret:  nt: namedtuple, query values accessible via `.key`
#       err: None|str, error state
def QueryUnpack(query, *keys, first_only=True):
    if len(keys) <= 0:
        return None, f"expected *keys"
    nt = namedtuple("nt", keys)
    l = []
    for k in keys:
        v = query.get(k)
        if not v:
            return None, f"missing key in query: {k}"
        if first_only: v = v[0]
        l.append(v)
    return nt(*l), None


# mkStr turns python strings into utf-8 bytestream
def MkStr(s):
    return bytes(s, encoding="utf-8")


# MkJSON turns python types into a JSON bytestream
# ret: bytes, error
def MkJSON(data):
    try:
        r = json.dumps(data, ensure_ascii=False).encode('utf-8')
    except Exception as err:
        return None, f"{err}"
    return r, None


# Fetch a file from the filesystem, return its content and mimetype
# FIXME: this should use an LRU cache
def _get_file(path):
    with open(path, "rb") as fp:
        b = fp.read()
        return b, mimetypes.guess_type(path)[0]


# Fetch a utf-8 template from a file, apply substitutions provided by func,
# return the result and a content-type. Benefits from _get_file's LRU caching
def _get_template(path, func):
    fb, ctype = _get_file(path)
    t = string.Template(fb.decode("utf-8"))
    return bytes(t.safe_substitute(func()), encoding="utf-8"), ctype


# JsonResponse takes a code and data and returns a handler-function return
# tuple. data must be a JSON-serializable object, otherwise an ErrorResponse is
# returned instead. This helps standardizing responses between API handler
# functions with different return-formats. The format is in line with the style
# of ErrorResponse. If the data cannot be json
def JsonResponse(code, data):
    j, err = MkJSON({"result": data})
    if err != None:
        return ErrorResponse(500, err)
    return j, code, CTYPE_JSON


# ErrorResponse takes a code and msg, and returns a handler-function return
# tuple. msg must be a JSON-serializeable object, otherwise an ErrorResponse
# resulting from the serialization is returned with code 500. This helps
# standardizing responses between API handler functions. The format is in line
# with the style of JsonResponse.
# ErrorResponse is HTCPCP (RFC2324) compliant.
def ErrorResponse(code, msg):
    if code == 418:
        phrase = "I'm a teapot"
    else:
        phrase = HTTPStatus(code).phrase
    j, err = MkJSON({"error": {"code": code, "phrase": phrase, "msg": msg}})
    if err != None:
        return ErrorResponse(500, err)
    return  j, code, CTYPE_JSON


# SetTimeout configures timeout for all ReadyServer instances
# par: t(int|None)
def SetTimeout(t):
    if ((isinstance(t, int) and t >= 0) or t == None):
        HTTPHandler.timeout = t
    else:
        raise ValueError(f"SetTimeout: bad param: {t}")


# SetMaxContentLength configures maximum content length in bytes for
# all ReadyServer instances
def SetMaxContentLength(l):
    if isinstance(l, int) and l >= 0:
        HTTPHandler.max_content_length = l
    else:
        raise ValueError(f"SetMaxContentLength: bad param: {l}")


# readyHTTP is an easy to configure RESTful API server instance with which
# handler-functions can be registered to method-path tuples
class ReadyServer():
    # initialize a server
    # lhost: local host addr (ip or URL)
    # lport: local port to use
    # log: has callable methods to log strings (debug, info, warn, error)
    def __init__(self, lhost, lport, log, name="server", tls=False, keyfile=None, certfile=None):
        self.server = ThreadingHTTP((lhost, lport), HTTPHandler)
        self.server.log = log
        if tls:
            self.server.socket = ssl.wrap_socket(
                self.server.socket,
                certfile = certfile,
                keyfile = keyfile,
                server_side = True)
        self.thread = threading.Thread(target = self.server.serve_forever)
        self.server._static_paths = { "GET": {}, "HEAD": {}, "POST": {}, "PUT": {}, "PATCH": {}, "DELETE": {} }
        self.server._glob_paths = { "GET": {}, "HEAD": {}, "POST": {}, "PUT": {}, "PATCH": {}, "DELETE": {} }
        self.server._name = name
        self.running = False

    # start server
    def Start(self):
        if self.running:
            raise RuntimeError(f"{self.server._name}: already running")
        self.thread.start()
        self.running = True

    # stop server
    def Stop(self):
        if not self.running:
            raise RuntimeError(f"{self.server._name}: not running")
        self.server.shutdown()
        self.running = False

    # IsAlive determines if the Server Thread is running
    def IsAlive(self):
        return self.thread.is_alive()

    # Register a handler to a path
    # par: method: str, the http method/verb
    #      handler_type: api, file, redirect, template
    #      handler: Depending on handler_type, can be a;
    #           function (type api)
    #           filepath (type file)
    #           path (type redirect)
    #           (filepath, function) (type template)
    def RegisterHandler(self, method, path, handler, handler_type):
        is_glob = False
        for pattern_elem in ["*", "?", "["]:
            if pattern_elem in path:
                is_glob = True

        if is_glob:
            paths = self.server._glob_paths[method]
        else:
            paths = self.server._static_paths[method]

        if path in paths:
            raise RuntimeError(f"RegisterHandler: path already registered: {method} {path}")

        paths[path] = (handler, handler_type)

    def RegisterGET(self, path, handler):
        self.RegisterHandler("GET", path, handler, "api")

    def RegisterHEAD(self, path, handler):
        self.RegisterHandler("HEAD", path, handler, "api")

    def RegisterPOST(self, path, handler):
        self.RegisterHandler("POST", path, handler, "api")

    def RegisterPUT(self, path, handler):
        self.RegisterHandler("PUT", path, handler, "api")

    def RegisterPATCH(self, path, handler):
        self.RegisterHandler("PATCH", path, handler, "api")

    def RegisterDELETE(self, path, handler):
        self.RegisterHandler("DELETE", path, handler, "api")

    # Register static file for service via GET or HEAD
    def RegisterStatic(self, path, alias=None):
        for pattern_elem in ["*", "?", "["]:
            if pattern_elem in path:
                raise RuntimeError("glob wildcards not allowed in RegisterStatic")
        self.RegisterHandler("GET", path, path[1:], "file")
        self.RegisterHandler("HEAD", path, path[1:], "file")
        if alias is not None:
            self.RegisterRedirect("GET", alias, path)
            self.RegisterRedirect("HEAD", alias, path)

    # Register static paths for all files found by globbing path
    # Accepts a string conforming to the glob-module
    def RegisterStaticGlob(self, path):
        # we work with directories here, but usually paths are provided
        # as relative web-paths, which start with /
        path=path.strip("/")
        # get list of all files
        files = glob.glob(path, recursive=True)
        for f in files:
            self.RegisterStatic(f"/{f}")

    # Register a redirect
    def RegisterRedirect(self, method, path, redir_path):
        for pattern_elem in ["*", "?", "["]:
            if pattern_elem in path:
                raise RuntimeError("glob wildcards not allowed in RegisterRedirect")
        self.RegisterHandler(method, path, redir_path, "redirect")

    # Register a template to be filled in by calling a function
    # par: path: path to register
    #      template: path to template file
    #      func: function that will provide template data
    def RegisterTemplate(self, path, template, func):
        for pattern_elem in ["*", "?", "["]:
            if pattern_elem in path:
                raise RuntimeError("glob wildcards not allowed in RegisterTemplate")
        self.RegisterHandler("GET", path, (template, func), "template")





# PrintLog is a dummy-logger for ReadyServer, printing all logmessages to stdout
class PrintLog():
    def doPrint(self, s):
        print(f"{time.time()} | {s}")
    def debug(self, *ss):
        self.doPrint(f"DEBUG | {' | '.join(ss)}")
    def info(self, *ss):
        self.doPrint(f"INFO  | {' | '.join(ss)}")
    def warn(self, *ss):
        self.doPrint(f"WARN  | {' | '.join(ss)}")
    def error(self, *ss):
        self.doPrint(f"ERROR | {' | '.join(ss)}")
