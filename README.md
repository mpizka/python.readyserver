# python.ReadyServer

ReadyServer provides a simple wrapper around http.server to get a RESTful
service up and running easily. Intended for use in fast prototyping and PoCs,
with minimal hassle to the user.


# project state

The project is currently **under development** & **pre-release**. While it
can be used, doing so is not yet recommended.


# quickstart-example

`example.py` is provided as a quickstart-example, showing off basic usage

`example_tls.py` uses TLS. Running it requires a certificate for localhost.
The helperscript `makecert.sh` can be used to generate it.


# class ReadyServer

`ReadyServer(lhost, lport, log, name="server", tls=False, keyfile=None, certfile=None)`

*lhost* & *lport* define the interface the server will listen on for incoming requests

*tls* determines if the server operates in TLS mode. *keyfile* and *certfile*
should be set if TLS is used.

*log* is the logger the server will use. The logger has to offer the methods
`error`, `warn`, `info` & `debug` which take an arbitrary number of strings
as params. `ReadyServer.PrintLog` can be used as a dummy-logger.

*name* can be set to an arbitrary string. The server will use it as prefix
in all logging messages.


## running the server

`Start()`: Run the servers listening thread
`Stop()` Stop the servers listening thread


## registering handlers

`RegisterGET(path, handler)`  
`RegisterHEAD(path, handler)`  
`RegisterPOST(path, handler)`  
`RegisterPUT(path, handler)`  
`RegisterPATCH(path, handler)`  
`RegisterDELETE(path, handler)`

These methods register an HTTP-verb sp:ecific handler to a path. `handler`
is a handler-function dealing with the request and returning a response,
path is a Path-instance (see below)


# handler functions

Handler functions must have the following signature:

`def foo(server, path, headers, query, data=None) -> (data, code, contentType)`  

`server` is the ReadyServer request.handler-instance that received the
request, `path` (str) is the requests URL path (useful for re.pattern paths,
see below), `headers` (dict) contains the requests headers, `query` (dict)
contains URL query params in the format `{key:[val1,...],...}`, data(bytes)
is the request-body (if exists).

All params are passed by name, allowing for brevity in handler-definitions
when not all params are required, e.g. `def foo(query, **kwargs)`

Handler functions return the 3-tuple `(response-data, response-code,
content-type)`. The module offers often-used content types as constants
e.g. `CTYPE_JSON`. The server will assume UTF-8 encoding for all text-based
content-types (JSON, HTML, etc.)

If an exceptions raises from a handler to the server, an error-response is
sent back as a JSON object.

Handler functions can add additional headers if required, by invoking
`server.AddHeader(key,val)` before returning. These headers will only be sent
if no exception is raised. If key/val are not str, `AddHeader` will do nothing.

The following content-type constants are implemented:
`CTYPE_JSON`, `CTYPE_TEXT`, `CTYPE_HTML`, `CTYPE_BYTES`, `CTYPE_NONE`

If `response-data` is type int, no body data will be sent, but `Content-Length`
header will be set to that int value. This is useful for handlers reacting to
`HEAD` requests. Such handlers may still return a byte-stream, in which
case the server will determine the content-length, but will not send the
data as body.


# module utility functions

`ErrorResponse(code, msg)`: returns a 3-tuple to be sent as an error response
in accordance to the servers internal error responses. RFC 2324 compliant.

`MkJSON(data) -> bytes, error`: get JSON byte stream from python type (data).
If `error != None`, the conversion failed.

`MkStr(s) -> bytes`: get bytestream from string

`QueryUnpack(query, *keys, first_only=True) -> nt, err`: returns a named tuple
with attributes `keys` and the corresponding values from the URL-query.
If `first_only` is set false, the values will be `[str]`, otherwise `str`
`err` is != `None` if a key is missing. Only works for keys which are valid
python identifiers.


# paths

Handler funcs register to url-paths. Incoming requests are handed to the first
handler of matching command (HTTP Verb), whos path matches the requests url
(by order of registration). Paths may be type `str` or ``e.Pattern.


# timeout & max content length

A timeout for inbound connections can be configured, as well as a maximum
permitted content-length using `SetTimeout(t)` & `SetMaxContentLength(l)`. Both
must be called before a `ReadyServer` is instanciated.

Defaults: no timeout, 16 MiB max content-length


# contact

Manfred Pizka: mpizka@icloud.com
