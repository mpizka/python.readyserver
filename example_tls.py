# example with tls
# requires `cert.pem` and `key.pem` in same directory

import re

import logging
import ReadyServer as rs


logging.basicConfig(level="DEBUG")


# isAliveHandler simply returns HTTP:200 indicating the server is running
def isAliveHandler(server, path, query, headers, data=None):
    return b"", 200, rs.CTYPE_TEXT


# echoHandler returns a JSON Object detailing the request
def echoHandler(server, path, query, headers, data=None):
    r = {"path": path, "query": query, "headers": headers}
    data, err = rs.MkJSON(r)
    return data, 200, rs.CTYPE_JSON


# This shows the backend of a very basic webapp, allowing you to subtract
# lives of the cat (why would you want to do that? :-[, showing remaining
# lives and resetting the cat, demonstrating handling of internal state
# of a backend using ReadyServer
CAT = {"lives": 9}

def lives():
    return "\u2665 " * CAT["lives"]

# `/reset` resets the cat to full live
def resetCatHandler(server, path, query, headers, data=None):
    global CAT
    CAT["lives"] = 9
    return rs.mkStr(f"Cat-Lives: {lives()}"), 200, rs.CTYPE_TEXT

# `/subtract?lives=n` subtracts n lives from the cat
def minusCatHandler(server, path, query, headers, data=None):
    global CAT
    val = 1
    if "lives" in query: val = int(query["lives"][0])
    if CAT["lives"] < val:
        return rs.mkStr(f"Cat-Lives: {lives()}\ncannot subtract, too few lives"), 200, rs.CTYPE_TEXT
    CAT["lives"] -= val
    return rs.mkStr(f"Cat-Lives: {lives()}"), 200, rs.CTYPE_TEXT

# `/show` just displays the cats remaining lives
def showCatHandler(server, path, query, headers, data=None):
    return rs.mkStr(f"Cat-Lives: {lives()}"), 200, rs.CTYPE_TEXT


# faviconHandler handles the default `/favicon.ico` request some browsers
# make to get the page-icon. This shows how to send arbitrary data back,
# and how handler definitions can be succinct if they don't need the details
# about the request
def faviconHandler(**kwargs):
    with open("favicon.ico", "rb") as fp:
        icon = fp.read()
    return icon, 200, rs.CTYPE_BYTES


# defaultHandler takes care of  all requests to paths not meeting any
# other path-definitions. The server can handle such "nopath" requests
# automatically by sending out an error-response-JSON.
def defaultHandler(path, **kwargs):
    return bytes(f"error 404\nsorry, but the '{path}' you seek is in another castle", encoding="utf-8"), 404, rs.CTYPE_TEXT


# instatiate a server
srv = rs.ReadyServer(lhost="127.0.0.1", lport=9042, log = logging, tls = True, certfile = "cert.pem", keyfile = "key.pem")

# Register the handlers to the server
srv.RegisterGET("/isalive", isAliveHandler)
# handler for all requests to /echo
srv.RegisterGET(re.compile("/echo(/.*)?"), echoHandler)
srv.RegisterGET("/reset", resetCatHandler)
srv.RegisterGET("/subtract", minusCatHandler)
srv.RegisterGET("/show", showCatHandler)
srv.RegisterGET("/favicon.ico", faviconHandler)
# the defaultHandler needs to be registered last, otherwise its "catch-all"
# path would match for every single GET request. Path-Matches are checked by
# order of registration
srv.RegisterGET(re.compile("/.*"), defaultHandler)

# run the server
srv.Start()
